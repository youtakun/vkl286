; NEWBS 1st stage loader written by Youta Sasaki(kazura@librem.one)
; Private and testing program. All rights reserved.

	ORG	$7C00

	JMP	OTINIT
	NOP

; BIOS BPB

	DB	"PFKIRDSY"
	DW	512
	TIMES	11	DB	0
	DW	18
	DW	2
	TIMES	4	DB	0
	DD	2880
	DB	0,0,$29
	TIMES	51	DB	0

; IPL

OTINIT:
	XOR	AX,AX
	CLI
	MOV	ES,AX
	MOV	SS,AX
	MOV	DS,AX
	MOV	SP,0X8000
	STI
	CLD

	MOV	CH, 0
	MOV	DH, 0
	MOV	CL, 2

	PUSH	0	; 5 >>0<< DO
L1_DO:
	MOV	AH, $02
	MOV	AL, 3
	MOV	BX, $0500
	MOV	DL, $00
	INT	$013
	JNC	L3_DO
	POP	AX
	INC	AX
	CMP	AX, 6	; >>5<< 0 DO
	JAE	L2_DO
	PUSH	AX	; >>1<< +LOOP
	XOR	AX, AX
	MOV	DL, $00
	INT	$013
	JMP	L1_DO	; 1 >>+LOOP<<

L2_DO:
	MOV	AH, $0E
	MOV	AL, 13
	XOR	BX, BX
	INT	$10
	MOV	AH, $0E
	MOV	AL, 10
	XOR	BX, BX
	INT	$10

	MOV	SI, FSTR_ERR	; FSTR_ERR is Pascal string addr
	LODSW	; sc_addr COUNT
	MOV	CX, AX	; c_addr >>u_len<< TYPE
L1_HL:
	LODSB
	MOV	AH, $0E
	XOR	BX, BX
	INT	$10
	LOOP	L1_HL

	JMP	HUNGUP

L3_DO:
	CLI	; Setting Segmentation for 2nd stage code
	MOV	AX, $0800	; Dataseg starts at $8000
	MOV	DS, AX
	MOV	AX, $0F00	; Extseg starts at $F000
	MOV	ES, AX
	MOV	AX, $0C00	; Stackseg starts at $C000
	MOV	SS, AX
	MOV	SP, $3000	; R-Stack and TIB allot $D000-$F000
	MOV	BP, $1000	; S-Stack allot $C000-$D000 (4KiB)
	STI	; End of setting segment and R and S stack
	JMP	$0000:$0500	; Codeseg is flat and starts at $500

HUNGUP:
	HLT
	JMP HUNGUP

FSTR_ERR:
	DW	10
	DB	"LOAD ERROR"
