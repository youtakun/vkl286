NASM=nasm
RM=rm
MV=mv
DD=dd
CP=cp
ASMTEST_OBJ=asmtest
COMAR_OBJ=\
	INITLOAD.COM\
	RDINIT.COM\
	INTERRPT.COM\
	VGA.COM

all: fdimage

fdimage: newbs comar
	$(CP) fdskel.dat fdimage.bin
	$(DD) if=NEWBS.BIN of=fdimage.bin conv=notrunc
	$(DD) if=COMAR.BIN of=fdimage.bin seek=1 conv=notrunc

comtest: COMTEST.COM

newbs: NEWBS.COM
	$(CP) bootsig.dat NEWBS.BIN
	$(DD) if=$< of=NEWBS.BIN conv=notrunc

comar: $(COMAR_OBJ)
	$(DD) if=/dev/zero of=COMAR.BIN bs=512 count=59
	$(DD) if=INITLOAD.COM of=COMAR.BIN conv=notrunc
	$(DD) if=RDINIT.COM of=COMAR.BIN seek=6 conv=notrunc
	$(DD) if=INTERRPT.COM of=COMAR.BIN seek=19 conv=notrunc
	$(DD) if=VGA.COM of=COMAR.BIN seek=27 conv=notrunc

clean: 
	$(RM) -f *.lst *.COM
	$(RM) -f NEWBS.BIN
	$(RM) -f COMAR.BIN
	$(RM) -f fdimage.bin

%.COM: %.ASM
	$(NASM) -o $@ -l $@.lst $<

%.C32: %.ASM
	$(NASM) -o $@ -l $@.lst $<
